﻿using System;
using System.IO;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace CTVoicer.Poc.SQS.Publisher.Consumer.EventBus
{
    class Program
    {
        private static readonly IConfiguration Configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetParent(AppContext.BaseDirectory)?.FullName)
            .AddJsonFile("appsettings.json")
            .Build();

        static async Task Main(string[] args)
        {
            await Host.CreateDefaultBuilder(args)
                .ConfigureServices((_, services) =>
                {
                    services.AddMassTransit(x =>
                    {
                        x.UsingAmazonSqs((context, cfg) =>
                        {
                            cfg.Host(Configuration["AmazonSqs:Region"], h =>
                            {
                                h.AccessKey(Configuration["AmazonSqs:AccessKey"]);
                                h.SecretKey(Configuration["AmazonSqs:SecretKey"]);
                            });
                            
                            cfg.ConfigureEndpoints(context);
                        });
                    });
            
                    services.AddMassTransitHostedService(true);
                    
                    services.AddHostedService<ConsoleHostedService>();
                })
                .RunConsoleAsync();
        }
    }
}