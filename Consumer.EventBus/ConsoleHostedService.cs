﻿using System;
using System.Threading;
using System.Threading.Tasks;
using CTVoicer.Poc.SQS.Domain.Models;
using MassTransit;
using Microsoft.Extensions.Hosting;

namespace CTVoicer.Poc.SQS.Publisher.Consumer.EventBus
{
    public class ConsoleHostedService : IHostedService
    {
        private readonly IBus _bus;

        public ConsoleHostedService(IBus bus)
        {
            _bus = bus;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Request Queue: " + DateTime.Now);
            
            var client = _bus.CreateRequestClient<SimpleMessageRequest>(new Uri("queue:EventBusQueue"));
            var response = await client.GetResponse<SimpleMessageResponse>(new SimpleMessageRequest { Text = "Hellooo!!" }, cancellationToken);
            
            Console.WriteLine("Response: " + response.Message.Message);
            Console.WriteLine();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}