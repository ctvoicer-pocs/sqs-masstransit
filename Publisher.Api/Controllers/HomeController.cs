﻿using System;
using System.Threading.Tasks;
using CTVoicer.Poc.SQS.Domain.Models;
using MassTransit;
using Microsoft.AspNetCore.Mvc;

namespace CTVoicer.Poc.SQS.Publisher.Api.Controllers
{
    [Route("api/send-to-queue")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IBus _bus;

        public HomeController(IBus bus)
        {
            _bus = bus;
        }

        [HttpPost]
        public async Task<IActionResult> Post(SimpleMessageRequest request)
        {
            var queue = await _bus.GetSendEndpoint(new Uri("queue:SimpleQueue"));
            await queue.Send(request);

            return Ok();
        }
    }
}