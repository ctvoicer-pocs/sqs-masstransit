﻿using System;
using System.Threading.Tasks;
using CTVoicer.Poc.SQS.Domain.Models;
using MassTransit;

namespace CTVoicer.Poc.SQS.Publisher.Api.Handlers
{
    public class EventBusConsumer : IConsumer<SimpleMessageRequest>
    {
        public async Task Consume(ConsumeContext<SimpleMessageRequest> context)
        {
            await context.RespondAsync(new SimpleMessageResponse
            {
                Status = 200, Message = $"Ok - {DateTime.Now}"
            });
        }
    }
}