using CTVoicer.Poc.SQS.Publisher.Api.Handlers;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace CTVoicer.Poc.SQS.Publisher.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            
            services.AddMassTransit(x =>
            {
                x.AddConsumer<EventBusConsumer>();

                x.UsingAmazonSqs((context, cfg) =>
                {
                    cfg.Host(Configuration["AmazonSqs:Region"], h =>
                    {
                        h.AccessKey(Configuration["AmazonSqs:AccessKey"]);
                        h.SecretKey(Configuration["AmazonSqs:SecretKey"]);
                    });

                    cfg.ReceiveEndpoint("EventBusQueue", e =>
                    {
                        e.ConfigureConsumeTopology = false;
                        e.ConfigureConsumer(context, typeof(EventBusConsumer));
                    });

                    cfg.ConfigureEndpoints(context);
                });
            });
            
            services.AddMassTransitHostedService();
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Publisher.Api", Version = "v1" });
            });
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime applicationLifetime)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Publisher.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}