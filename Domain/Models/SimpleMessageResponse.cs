﻿namespace CTVoicer.Poc.SQS.Domain.Models
{
    public class SimpleMessageResponse
    {
        public int Status { get; set; }
        
        public string Message { get; set; }
    }
}