﻿namespace CTVoicer.Poc.SQS.Domain.Models
{
    public class SimpleMessageRequest
    {
        public string Text { get; set; }
    }
}