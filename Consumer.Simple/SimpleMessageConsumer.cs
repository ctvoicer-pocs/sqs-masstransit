﻿using System;
using System.Threading.Tasks;
using CTVoicer.Poc.SQS.Domain.Models;
using MassTransit;

namespace CTVoicer.Poc.SQS.Publisher.Consumer.Simple
{
    public class SimpleMessageConsumer : IConsumer<SimpleMessageRequest>
    {
        public Task Consume(ConsumeContext<SimpleMessageRequest> context)
        {
            Console.WriteLine($"Recived the Queue Message: {context.Message.Text}");
            
            return Task.CompletedTask;
        }
    }
}