﻿using System;
using System.IO;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace CTVoicer.Poc.SQS.Publisher.Consumer.Simple
{
    class Program
    {
        private static readonly IConfiguration Configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetParent(AppContext.BaseDirectory)?.FullName)
            .AddJsonFile("appsettings.json")
            .Build();
        
        static async Task Main(string[] args)
        {
            Console.WriteLine("Consumer das Filas do projeto 'Publisher.Api' iniciado...");
            Console.WriteLine("Utilize a API do Publisher.Api para envio de mensagens a fila...");
            Console.WriteLine();
            
            await Host.CreateDefaultBuilder(args)
                .ConfigureServices((_, services) =>
                {
                    services.AddMassTransit(x =>
                    {
                        x.UsingAmazonSqs((_, cfg) =>
                        {
                            cfg.Host(Configuration["AmazonSqs:Region"], host =>
                            {
                                host.AccessKey(Configuration["AmazonSqs:AccessKey"]);
                                host.SecretKey(Configuration["AmazonSqs:SecretKey"]);
                            });
                
                            cfg.ReceiveEndpoint("SimpleQueue", endpoint =>
                            {
                                endpoint.Consumer(() => new SimpleMessageConsumer());
                            });
                        });
                    });
            
                    services.AddMassTransitHostedService(true);
                })
                .RunConsoleAsync();
        }
    }
}